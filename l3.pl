%l3
/* 
Задание:
Разработать предикат  FIRST для определения первого элемента списка 
(в учет принимаются списки всех уровней).  
Пример:  исходный  список: [[[s,a],b],c,[e,[f,[[g,e,p],v],y]],x]. 
Первый элемент: s. 
Для отладки программ в SWI Prolog используйте предикат trace.
*/

first([H],H):-atom(H).
first([H|_],X):-first(H,X),atom(X),!.
first([H|_],X):-first([H|T],X), !.