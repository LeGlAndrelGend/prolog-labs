%l2
:- discontiguous(man/1).
:- discontiguous(woman/1).
:- discontiguous(married/2).
:- discontiguous(parent/2).

/*
-----------------------------------------
 Номер               Шифры
варианта          родственных отношений
-----------------------------------------
   1            А1,А11,Б7,Б17,Б24,С1
   2            А2,А12,Б8,Б18,Б25,С2
   3            А3,А13,Б9,Б19,Б23,С10
   4            А4,А14,Б10,Б20,С9,С11
   5            А5,Б1,Б11,Б21,С8,С12
   6            А6,Б2,Б12,Б22,С7
   7            А7,Б3,Б13,Б26,С6
   8            А8,Б4,Б14,Б27,С5
   9            А9,Б5,Б15,Б28,С4
  10            А10,Б6,Б16,Б29,С3
-----------------------------------------
*/

%Вариант - 1            А1,А11,Б7,Б17,Б24,С1
%A1 (отец)
father(Father, Child) :- man(Father), parent(Father, Child).	

%A11 (тетя)
sibling(X, Y) :- parent(Z, X), parent(Z, Y), dif(X, Y).
aunt(Aunt, N) :- woman(Aunt), sibling(X, Aunt), parent(X, N).

%Б7 (троюродный брат)
grandparent(GParent, X) :- parent(GParent, Parent), parent(Parent, X).
grandchild(GChild, X) :- parent(X, Parent), parent(Parent, GChild).
second_coustin_brother(SCBro, X) :- grandparent(GParent, X), sibling(GParent, Sib), 
   parent(Sib, Y), parent(Y, SCBro), man(SCBro).

%Б17 (двоюродный племянник)
first_coustin(FCoustin, X) :- parent(Parent, X), sibling(Parent, Sib), 
   parent(Sib, FCoustin), man(FCoustin).
first_coustin_nephew(FCNephew, X) :- first_coustin(X, Y), 
   parent(Y, FCNephew), man(FCNephew).

%Б24 ((N)-юродный брат (любого уровня))
any_coustin_brother(CBrother, X) :- parent(Parent, X), sibling(Parent, Sib),
   parent(Sib, CBrother).
any_coustin_brother(ACBrother, X) :- parent(Y, ACBrother), parent(Z, X), 
   man(ACBrother), any_coustin_brother(Y, Z). 

%C1 (муж)
husband(Husband, X) :- married(Husband, X), man(Husband).

/* Тестовые данные START*/

man(x_x_fedor).
parent(x_x_fedor, f_x_kolya).

man(x_x_misha).
parent(x_x_misha, m_x_lilya).
parent(x_x_misha, m_x_gena).

man(x_x_lesha).

parent(x_x_lesha, l_x_sveta).

man(f_x_kolya).
woman(m_x_lilya). 
married(f_x_kolya, m_x_lilya).
parent(f_x_kolya, k_l_volodya).
parent(f_x_kolya, k_l_valya).
parent(m_x_lilya, k_l_volodya).
parent(m_x_lilya, k_l_valya).

man(m_x_gena).
woman(x_x_natalya).
married(m_x_gena, x_x_natalya).
parent(m_x_gena, g_x_tanya). 
parent(m_x_gena, g_x_dima).
parent(x_x_natalya, g_x_dima).
parent(x_x_natalya, g_x_dima).

man(k_l_valya).
woman(l_x_sveta).
married(k_l_valya, l_x_sveta).
parent(k_l_valya, v_s_andrei).
parent(k_l_valya, v_s_sasha).
parent(k_l_valya, v_s_nastya).
parent(l_x_sveta, v_s_andrei).
parent(l_x_sveta, v_s_sasha).
parent(l_x_sveta, v_s_nastya).

man(k_l_volodya).
woman(x_x_lena). 
married(k_l_volodya, x_x_lena).
parent(k_l_volodya, v_l_katya).
parent(x_x_lena, v_l_katya).

woman(g_x_tanya). 

man(g_x_dima).
woman(x_x_firstWifeDima). 
married(g_x_dima, x_x_firstWifeDima).
parent(g_x_dima, d_x_kristina).

woman(x_x_oksana).
married(g_x_dima, x_x_oksana).
parent(g_x_dima, d_x_nastya).
parent(g_x_dima, d_x_irina).
parent(x_x_oksana, d_x_nastya).
parent(x_x_oksana, d_x_irina).

man(x_x_sergei).
woman(x_x_olga). 
married(g_x_sergei, x_x_olga).

man(v_s_andrei).
man(i_s_lilia). 

man(v_s_sasha).
woman(m_s_olya). 
married(v_s_sasha, m_s_olya).
parent(v_s_sasha, s_o_misha).
parent(m_s_olya, s_o_misha).

woman(v_s_nastya).

woman(v_l_katya).
man(x_x_vadim). 
married(x_x_vadim, v_l_katya).

man(s_o_misha).

woman(d_x_kristina).
woman(d_x_nastya).
woman(d_x_irina).

/* Тестовые данные END*/