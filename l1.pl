﻿parent(pame,bob).
parent(tom,bob).
parent(tom,lize).
parent(bob,anna).
parent(bob,pate).
parent(pate,jhim).

woman(pame).
woman(lize).
woman(pate).
woman(anna).
man(tom).
man(bob).
man(jhim).

mother(X) :- woman(X), parent(X, Child).

predok(Parent, Child) :- parent(Parent, Child).
predok(GrandP, Child) :- parent(GrandP, Parent), predok(Parent, Child). 
