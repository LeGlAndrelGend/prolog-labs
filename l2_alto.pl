:- style_check(-singleton).
:- discontiguous axis/5.
:- discontiguous axis_unlim/5.
:- discontiguous cyl/2.

/*test data begin*/
cyl(c1, 1).
cyl(c2, 2).
cyl(c3, 3).
cyl(c4, 4).
cyl(c5, 5).
/*test data end*/
ext(Cyl, Ext) :- 
	cyl(Cyl, Len),
	Ext is Len.

ext(Cyl, Ext) :-
	cyl(Cyl, Len),
	Ext is 2 * Len - 1.

/* axis(CurList, CumulExt, UncheckedList, ResList, TargetExt), where:
* CurList - the current list of selected cylinders
* CumulExt - the cumulative extension of the current selected cylinders
* UncheckedList - the list of unchecked cylinders
* ResList - the result list of the selected cylinders
* TargetExt - the desired cumulative extension length of cylinders
*/

/*Founded*/
axis(CurList, CumulExt, [], CurList, TargetExt) :-
	CumulExt is TargetExt.
/*Not founded*/
axis(_, CumulExt, [], _, TargetExt) :-
	TargetExt \= CumulExt,
	fail.
/*skip element*/
axis(CurList, CumulExt, [_|Tail], ResList, TargetExt) :-
	axis(CurList, CumulExt, Tail, ResList, TargetExt).
/*include, if not lead to extension excess*/
axis(CurList, CumulExt, [Head|Tail], ResList, TargetExt) :-
	ext(Head, HeadExt),
	NewCumulExt is CumulExt + HeadExt,
	NewCumulExt =< TargetExt, 
	axis([Head|CurList], NewCumulExt, Tail, ResList, TargetExt).

find_cyl_axis(TargetExt, ResList) :- 
/*set UncheckedList all available cylinders*/
	findall(X, cyl(X,_), UncheckedList),
	axis([], 0, UncheckedList, ResList, TargetExt).
unreachable_tray_exists :- 
	findall(X, cyl(X, _), UncheckedList),
	tray(TargetExt),
	not(axis([], 0, UncheckedList, _, TargetExt)).

/*----------------------------------------------------------------
-----------------------------TASKS--------------------------------
----------------------------------------------------------------*/

/*--task_1--*/

find_all_trays(ResList, TargetExt) :-
	axis([], 0, ResList, Configs, TargetExt),
	writeln(Configs).

/*
find_all_trays_2(ResList, TargetExt) :-
	axis([], 0, [c1, c2, c3], ResList, TargetExt).
*/

/*--task_2--*/
new_ext(Cyl, Len) :-
	cyl(Cyl, LenCyl),
	ext(Cyl, Ext),
	between(LenCyl, Ext, Len).

/*--task_3--*/
/*test data begin*/
hcyl(h1, 3).
hcyl(h2, 4).
vcyl(v1, 2).
vcyl(v2, 3).
tray(4, 3).
tray(4, 2).

/*unreachable*/
tray(0, 0).
tray(1, 0).
tray(1, 1).
tray(2, 1).
tray(2, 2).
tray(3, 2).
tray(3, 3).
tray(100, 3).
tray(4, 100).
/*test data end*/
ext_h(Cyl, Ext) :- 
	hcyl(Cyl, Len),
	Ext is Len.

ext_h(Cyl, Ext) :-
	hcyl(Cyl, Len),
	Ext is 2 * Len - 1.

new_ext_h(Cyl, Len) :-
	hcyl(Cyl, LenCyl),
	ext_h(Cyl, Ext),
	between(LenCyl, Ext, Len).

/*Founded*/
axis_h(CurList, CumulExt, [], CurList, TargetExt) :-
	CumulExt is TargetExt.
/*Not founded*/
axis_h(_, CumulExt, [], _, TargetExt) :-
	TargetExt \= CumulExt,
	fail.
/*skip element*/
axis_h(CurList, CumulExt, [_|Tail], ResList, TargetExt) :-
	axis_h(CurList, CumulExt, Tail, ResList, TargetExt).
/*include, if not lead to extension excess*/
axis_h(CurList, CumulExt, [Head|Tail], ResList, TargetExt) :-
	new_ext_h(Head, HeadExt),
	NewCumulExt is CumulExt + HeadExt,
	NewCumulExt =< TargetExt, 
	axis_h([Head|CurList], NewCumulExt, Tail, ResList, TargetExt).

find_all_trays_h(ResList, TargetExt, Configs) :-
	axis_h([], 0, ResList, Configs, TargetExt).

ext_v(Cyl, Ext) :- 
	vcyl(Cyl, Len),
	Ext is Len.

ext_v(Cyl, Ext) :-
	vcyl(Cyl, Len),
	Ext is 2 * Len - 1.

new_ext_v(Cyl, Len) :-
	vcyl(Cyl, LenCyl),
	ext_v(Cyl, Ext),
	between(LenCyl, Ext, Len).

/*Founded*/
axis_v(CurList, CumulExt, [], CurList, TargetExt) :-
	CumulExt is TargetExt.
/*Not founded*/
axis_v(_, CumulExt, [], _, TargetExt) :-
	TargetExt \= CumulExt,
	fail.
/*skip element*/
axis_v(CurList, CumulExt, [_|Tail], ResList, TargetExt) :-
	axis_v(CurList, CumulExt, Tail, ResList, TargetExt).
/*include, if not lead to extension excess*/
axis_v(CurList, CumulExt, [Head|Tail], ResList, TargetExt) :-
	new_ext_v(Head, HeadExt),
	NewCumulExt is CumulExt + HeadExt,
	NewCumulExt =< TargetExt, 
	axis_v([Head|CurList], NewCumulExt, Tail, ResList, TargetExt).

find_all_trays_v(ResList, TargetExt, Configs) :-
	axis_v([], 0, ResList, Configs, TargetExt).

find_cyl_axis_xy(TargetExtX, TargetExtY, ResListX, ResListY) :- 
	find_all_trays_h(ResListX, TargetExtX, Configs1),
	find_all_trays_v(ResListY, TargetExtY, Configs2),
	write(Configs1), write(', '), writeln(Configs2). 	

unreachable_tray_xy_exists :- 
	findall(X, hcyl(X, _), UncheckedList1),
	findall(Y, vcyl(Y, _), UncheckedList2),
	tray(TargetExt1, TargetExt2),
	not(axis_h([], 0, UncheckedList1, _, TargetExt1)),
	not(axis_v([], 0, UncheckedList2, _, TargetExt2)).

/*--task_4--*/

/*test data begin*/
cyl(c, 5).
/*test data end*/

/*Founded*/
axis_unlim(CurList, CumulExt, [], CurList, TargetExt) :-
	CumulExt is TargetExt.
/*Not founded*/
axis(_, CumulExt, [], _, TargetExt) :-
	TargetExt \= CumulExt,
	fail.
/*skip element*/
axis_unlim(CurList, CumulExt, [_|Tail], ResList, TargetExt) :-
	axis_unlim(CurList, CumulExt, Tail, ResList, TargetExt).
/*include, if not lead to extension excess*/
axis_unlim(CurList, CumulExt, [Head|Tail], ResList, TargetExt) :-
	new_ext(Head, HeadExt),
	NewCumulExt is CumulExt + HeadExt,
	NewCumulExt =< TargetExt, 	
	axis_unlim([Head|CurList], NewCumulExt, Tail, ResList, TargetExt).
/*add dublicate*/
axis_unlim(CurList, CumulExt, [], ResList, TargetExt) :-
	memberchk(c, CurList),
	cyl(c, HeadExt),
	NewCumulExt is CumulExt + HeadExt,
	NewCumulExt =< TargetExt,
	axis_unlim([c|CurList], NewCumulExt, [], ResList, TargetExt).
find_cyl_axis_unlim(TargetExt, ResList) :- 
/*set UncheckedList all available cylinders*/
	findall(X, cyl(X,_), UncheckedList),
	axis_unlim([], 0, UncheckedList, ResList, TargetExt).
/*set manual configuration*/
	%% axis_unlim([], 0, [c, c1, c2,c3,c4, c5], ResList, TargetExt).